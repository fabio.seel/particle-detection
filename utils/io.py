import imageio
from io import BytesIO

def open_video(path, bytestream=False):
    if bytestream:
        with open(path, 'rb') as file: 
            content = file.read()
        vid = imageio.get_reader(BytesIO(content),  'ffmpeg')
    else:
        vid = imageio.get_reader(path, 'ffmpeg')
    return vid
