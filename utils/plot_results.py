from matplotlib import pyplot as plt

def show(vid, output, img_no, channel, result_labels):
    '''
    Show an img from a video and draw the results as a scatter plot for visual control.
        
        Parameters:
            vid (imageio.Reader): The video from which the frame shall be displayed
            output (list): List of the results from passing the video to the model
            img_no (int): frame to be displayed
            channel (int): which "channel" shall be displayed
            result_labels: name of the channels, used as title
    '''
    plt.rc('font', size=20) #controls default text size
    vid.set_image_index(img_no)
    image = vid.get_next_data()[:,:,0]
    plt.figure(figsize=(50,50*image.shape[0]/image.shape[1]-10))
    plt.title(result_labels[channel])
    plt.imshow(image, cmap='gray')
    plt.scatter(output[img_no][2],output[img_no][1],c=output[img_no][channel], cmap='cool_r', s=10)
    plt.colorbar()
    plt.show()