import torch
import torch.nn as nn

def peak_local_max(input, threshold_abs=1, min_distance=1):
    '''
    Returns a binary map where maxima positions are true.

        Parameters:
            input (pytorch tensor): image-like pytorch tensor of dimension [batch_size, channels, width, height], where each image will be processed individually
            threshold_abs (float): local maxima below will be dropped
            min_distance (int): min distance (in pixels) between two maxima detections.
        Returns
            pytorch tensor of same shape as input
    '''
    max_filtered=nn.functional.max_pool2d(input, kernel_size=2*min_distance+1, stride=1, padding=min_distance)
    maxima = torch.eq(max_filtered, input)
    return maxima * (input >= threshold_abs)