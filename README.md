# Particle Detection
The purpose of this project is the deployment of the network trained for particle detection in "in-cloud-videos".
The network itself has been trained not only to find the particles, but also to collect further information (background intensity, image intensity, radial Defocus, ellipsoidal distortion).

## Setup
Install the conda environment and activate it:
> conda env create --name pytorch_cuda --file environment.yml
> 
> conda activate pytorch_cuda

This should include all important dependencies.

## Usage
Check example_usage.ipynb to see how to process a video.

To start it, make sure to have the cuda environment activated, switch to the project folder and type:
>jupyter-notebook

Alternatively, one can run jupyter notebooks nicely in VS Code: https://code.visualstudio.com/docs/datascience/jupyter-notebooks

## Performance

### Accuracy

The first thing to look at is if the particle positions are being predicted correctly. In this case, we count a correct prediction if it is within a radius of 1 Pixel from the groundtruth position.
The Precision over Recall curve shows, that a recall of almost 0.9 can be achieved when using a sufficiently low threshold. By qualitative analysis, it can be expected that the majority of the missing 10% are caused by impurity of the groundtruth data (actually, the model already starts reproducing some systematic errors from the groundtruth data)
_Note: the model output for this analysis has been "renormalized" to the range [0,255], while the direct model output is in the range [0,1]. For the threshold selection you either have to change the normalizing parameters (check the notebook), or divide the displayed threshold values by 255._

![](doc/validation_precision_over_recall.png)

The following plots show how well the additional information is predicted, by comparing the distribution of the groundtruth data (orange) with the predictions (blue), as well as a distribution of the "point wise" difference between the two.

![](doc/offsetY_hist.png)
![](doc/offsetX_hist.png)
![](doc/bg_intensity_hist.png)
![](doc/img_intensity_hist.png)
![](doc/radial_defocus_hist.png)
![](doc/ellipsoidal_distortion_hist.png)

### Speed

On a NVIDIA Geforce RTX 2080 Ti, the following processing speeds where achieved roughly for various batch sizes on average (just a rough estimation, no reliable measurements as other processes were running at the same time):

| Batch Size | FPS  |
|------------|------|
| 1          | 15.5 |
| 2          | 19.0 |
| 4          | 21.9 |
| 8          | 21.7 |
| 16         | 23.4 |